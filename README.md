

# **BLASTXplorer**

BLASTXplorer interface allows to visualize and explore the contents BLAST program output file depending on the fields available. The key visual interface of BLASTXplore is wordle visualization of BLAST description based on frequency, frequncy by position, etc., Further there are separate interfaces to visualize taxonomy information if provided and also the sequence alignment itself.



# **Browser Requirements**

BLASTXplorer uses JavaScript, HTML5 and svg element to visualize the phylogenetic tree. So it recommended to use modern browsers like chrome, firefox and safari, however, it is highly recommended to use Google Chrome as the application has been extensively tested in Chrome at the moment and also seem to offer the best performance.


# **Data format**

Currently the BLASTXplorer supports the <b> output format 7 </b> from command line applications. <b> Output format 7 </b> is tabular format with comment lines.
Below is the example of output format 7 with as much as fields as possible from command line app. Note that the visualization requires respective content be available on the output file, for example, taxonomy related info to render one of the hierarchical layout for taxonomy; similarly description field for word cloud.

![alt text](assets/outputFormat_1.png "DATA format")

![alt text](assets/outputFormat_2.png "DATA format")


# **Interface**

The main user interface comprises of following


### 1. File Input

The glyphicon on the navbar in the top right where the end user uploads the tree in "Newick" format as described above

### 2. Layout

Normally the div where the tree is rendered or displayed is in the center to the left of the paramters control and just below the navigation bar on the top

### 2.1. Visualization Display Area

Normally the div where the tree is rendered or displayed is in the center to the left of the paramters control and just below the navigation bar on the top

### 2.2. Parameters Control

Displays all html elements to control the tree related parameters in display level such as dendogram or regular phylogram; Also the input to collapse the depth of the tree and other styling attributes
depending on the display layout chosen


![alt text](assets/mainInterface.png "BLASTXplore User Interface")

Once the visualization layout is chosen the user will end up in the visual interface corresponding to the chosen design/layout and the organisation is consistent across the different design interfaces with parameter control on left and display area in the middle.


![alt text](assets/interface_2.png "Display User Interface")


# **Visualization Interface**

### 1. Taxonomy

Taxonomy allows hierarchical data to be explored with different visualization method suitable for hierarchical data. Charts can be created when relevant taxonomy information is provided in the output file uploaded. The example takes subject scientific names, blast names and kingdom to visualize the same.

#### 1.1 Sankey

<a href="https://en.wikipedia.org/wiki/Sankey_diagram"></a> Sankey layout is typically used for visualizing flow and here it is utilized for representing hierarchical flow of information.

#### 1.2 Circular Treemap

Circular treemap is a variation of treemap that uses circle instead of rectangles and is a space constrained visualization technique. Each containment represents a level in the hierarchy.


#### 1.3 Zoomable Pie Chart

Another space filling technique that uses radial layout and supports zooming to go deep in hierarchy.

#### 1.4 Treemap

<a href="http://www.cs.umd.edu/hcil/treemap-history/"></a> Treemap is a space constrained layout that uses nested rectangles to depict hierarchies.

![alt text](assets/taxonomy.png "taxonomy")


### 2. BLAST-Cloud (Wordle)

Creates word cloud for the BLAST subject description provided and works if the description field is available in the uploaded file. This is particularly useful to reveal unique hits that can be identified from textual information rather than having to go deep into the hit table which is time consuming.

Further the parameters can be controlled to keep only the first three or five words depending on the position chosen and also the frequency of occurrences.

![alt text](assets/wordle2.png "wordle")


### 3. SequenceViz (Alignment)

Here the subject sequences alignment can be visualized in horizontal bar layout and as individual plots or small multiples and coloring as per bit score

![alt text](assets/alignment_1.png "Sequence Alignment")


# **Code Flow & How it Works** #

As stated before BLASTXplorer makes use of browser to visualize and therefore we make use of JavaScript, SVG and Bootstrap for styling.

#### Key Dependencies
<ul>
<li> <a href="https://d3js.org/">D3.js </a>
<li> <a href="https://jquery.com/"> jQuery </a>
</ul>

All the external libraries used are under <b>vendor </b>directory for both css and js

The four key blocks of each app is "reading and parsing data", "apply initial settings", "core computations" and "render/draw". The flow is more or less the same across the visual layout JavaScript files

##### Reading and Parsing

The key file uploaded is normally in tabular format and is parsed first the parsing is done within <b>parseFile </b> function in input.js and then depending on the data type (taxonomy, word description, sequence) additional data processing is done to structure the data suitable for d3.js


```javascript
function parseFile(blastFile) {

           //regular expressions

          var parsedData = d3.tsv.parse(blastFile);


}

```



##### Initial Settings Block

The code within this applies default settings to the html elements in the sidebar (parameter controls) if necessary

```javascript

function initialSettings() {

       document.getElementById("depthSlider").value = maxDepth/2;
       render();
 }

```

##### Render/Draw

The render block actually draws the tree on browser leveraging mainly D3.js library and svg.
Render block also contains the function that does some action when a change is detected in any of the html input elements such as slider, button, etc.,

###### Example

````javascript

function render(){


     //append elements to svg depending on layout chosen

    //some actions example
       d3.select("#font").on("change", function () {

            var fontType = $("#font").val();

            d3.select("#chart").selectAll("text").style("font-family", fontType);

        });




    }
`````


